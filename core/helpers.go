package core

// ArrayUtil generic functions
type ArrayUtil []interface{}

// Contains function for ArrayUtil to generically see if the "needle" is in
// the "haystack" array. Interface types must be the same.
func (ac *ArrayUtil) Contains(haystack []interface{}, needle interface{}) bool {
	for _, search := range haystack {
		if search == needle {
			return true
		}
	}

	return false
}
